package com.baldorado; // the package where the Java class belongs to

//The main entry point of our program
public class Main {
    // access Modifier --> public
    // Return Type --> void
    // Pattern to create a function (method):
    /*
        accessModifier static returnType functionName(dataType argument) {
            // code blocks
        }
    */


    public static void main(String[] args) {
	// write your code here
        System.out.println("** Variables **");

        //Variables
        /*
            Syntax:
            dataType identifier = value;
        **/

        // Declare a variable w/o initialization
        int firstNum;

        //Declare a variable w/ initialization
        int secondNum =  23;

        // Initialize a variable after declaration
        firstNum = 34;

        // Variable reassignment
        secondNum = 24;

        // Constants

        /*
               Syntax:
               Final dataType identifier = value;
        * */

        final float INTEREST = 0.02F;
    }
}
