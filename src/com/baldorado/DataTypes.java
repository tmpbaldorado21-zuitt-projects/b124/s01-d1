package com.baldorado;

import java.util.Scanner;

public class DataTypes {
    public static void main(String[] args) {
        System.out.println("** Data Type **");

        //Data Type

        // 1) Primitive (Built-in) --> it stores simple values

        //byte age = 30;

        long worldPopulation = 7_862_081_145L; //L ---> to tell Java this is Long and not integer

        System.out.println("The current world population is " + worldPopulation);

        //float price = 12.99F; //F --> to tell Java this is float and not double

        //char firstLetter = 'A'; //Use single quotes to initialize a character

        //boolean isMarried = false;

        // 2) Non-Primitive (Referencing) --> it stores complex data (objects)

        //String completeName = "Tricia Mae Baldorado"; //short-hand
        //String completeName = new String ("Tricia Mae Baldorado");//long-hand

        //Mini Activity: Display to the console the current value of completeName/

        //System.out.println(completeName);

        //completeName.toLowerCase();// this invocation will return a string
       // System.out.println(completeName.toLowerCase());
        //String lowerCasedCompleteName = completeName.toLowerCase();
       // System.out.println(lowerCasedCompleteName);

        //Escape Sequence
        //System.out.println("C:\\Windows\\Desktop");
        //System.out.println("I am \"Software\" Developer");

        //Arithmetic Expressions
        int sum = 10 + 5;
        int product = 23 * 2;
        int difference = 34 - 30;
        double quotient = 100.0 / 25;
        System.out.println("10 + 5 = " + sum);
        System.out.println("23 * 2 = " + product);
        System.out.println("34 - 30 = " + difference);
        System.out.println("100.0 / 25 = " + quotient);

        //Casting (Type Casting)
        //Conversion from one data type to another

        //1) Implicit (Automatic) Casting
        //byte > short > int > long > float > double
        //This will work in ascending order
        int num1 = 5;
        double num2 = 2.7;
        double total = num1 + num2;
        System.out.println("5 + 2.7 = " + total);

        //2) Explicit Casting
        int num3 = 5;
        double num4 = 2.7;
        int total2 = num3 + (int) num4;
        System.out.println("5 + 2.7 = " + total2);

        //Converting Strings to Integers
        //String mathGrade = "96";
        //String englishGrade = "85";
        //System.out.println("Total Grade is: " + mathGrade + englishGrade);
       // System.out.println("Total Grade is: " +(Integer.parseInt(mathGrade))+ Integer.parseInt(englishGrade));
       // int totalGrade = Integer.parseInt(mathGrade) + Integer.parseInt(englishGrade);
       // System.out.println(Integer.toString(totalGrade));

        //Asking for input from a user
        //Scanner appScanner = new Scanner(System.in);

        //String firstName;
        //int ageInput;

        //System.out.println("What is your firstname?");
        //firstName = appScanner.nextLine().trim();
        //System.out.println("Hello, " + firstName + "! Welcome to the World of Java Programming.");

        //Mini-Activity;
        //System.out.println("What is your age?");
        //ageInput = appScanner.nextInt();
        //System.out.println("Hello, I am " + ageInput + "years old");

        //Activity 1:
        String firstName;
        String lastName;
        double engGrade;
        double mathGrade;
        double sciGrade;

        Scanner appScan = new Scanner(System.in);

        System.out.println("Enter your firstname:");
        firstName = appScan.nextLine();

        System.out.println("Enter your lastname:");
        lastName = appScan.nextLine();

        System.out.println("Enter your grade in English:");
        engGrade = appScan.nextDouble();

        System.out.println("Enter your grade in Math:");
        mathGrade = appScan.nextDouble();

        System.out.println("Enter your grade in Science:");
        sciGrade = appScan.nextDouble();

        System.out.println("My name is " + firstName  + lastName + "and my grade is " + (engGrade + mathGrade + sciGrade)/3);


    }
}